from django.shortcuts import render
from .models import TvshowProgramLang

def Tvshow_views(request):
    films = TvshowProgramLang.objects.all()
    return render(request, 'tvshow/tvshow.html',
                  {'films': films})

def Tvshow_detail_views(request, pk):
    film = TvshowProgramLang.objects.get(pk=pk)
    return render(request, 'tvshow_detail.html', {'film': film})


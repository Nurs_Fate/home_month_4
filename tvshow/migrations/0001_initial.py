# Generated by Django 4.2.4 on 2023-08-16 14:34

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='TvshowProgramLang',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=200)),
                ('release_date', models.DateField()),
                ('genre', models.CharField(choices=[('Аниме', 'Аниме'), ('Комедия', 'Комедия'), ('Драма', 'Драма')], max_length=20)),
                ('description', models.TextField()),
                ('director', models.CharField(max_length=100)),
                ('rating', models.FloatField()),
            ],
        ),
    ]

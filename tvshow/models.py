from django.db import models



class TvshowProgramLang(models.Model):
    GENRE_CHOICES = (
        ('Аниме', 'Аниме'),
        ('Комедия', 'Комедия'),
        ('Драма', 'Драма'),
    )

    title = models.CharField(max_length=200)
    image = models.ImageField(upload_to='', null=True)
    release_date = models.DateField()
    genre = models.CharField(max_length=20, choices=GENRE_CHOICES)
    description = models.TextField()
    director = models.CharField(max_length=100)
    rating = models.FloatField()

    def __str__(self):
        return self.title
